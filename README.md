## Introdução ao mundo DevOps!


## DevOps Roadmap

[![](https://img.shields.io/badge/-Download%20PDF%20-0a0a0a.svg?style=flat&colorA=0a0a0a)](https://gum.co/devops-roadmap) [![](https://img.shields.io/badge/-Shareable%20Link%20-0a0a0a.svg?style=flat&colorA=0a0a0a)](https://roadmap.sh/devops)

![DevOps Roadmap](./devops.png)

## 1. Vídeos

### 10 Deploys per day at Flickr

Esse palestra foi a ignição que faltava para a criação do movimento/cultura DevOps em 2009.

    https://www.youtube.com/watch?v=LdOe18KhtT4

### Entenda as origens DevOps

Vídeo rápido de Damon Edwards (devopsdays core member) acerca das origens do movimento, recomendo.

    https://www.youtube.com/watch?v=o7-IuYS0iSE

### What is DevOps? - In Simple English

Vídeo curtinho do rackspace que também ajuda.

    https://www.youtube.com/watch?v=_I94-tJlovg

### DevOpsConf - DevOps State of the Union (John Willis)

Palestra do John Willis (devopsdays core member) na DevOpsCon 2015 sobre o estado do DevOps após 5 anos de seu surgimento.

    https://www.youtube.com/watch?v=8rM8lYaMVBE

### Five Years of DevOps (Patrick Debois)

Patrick Debois o criador do DevOpsDays e do termo DevOps fala o que achou dos últimos 5 anos de DevOps.

    https://www.youtube.com/watch?v=uRMV6tT_mu0

## 2. Acrônimos

Entenda o que é CAMS

    http://devopsdictionary.com/wiki/CAMS

Entenda o que é CALMS

    http://whatis.techtarget.com/definition/CALMS

Entenda o que é ICE

    http://radar.oreilly.com/2015/01/devops-keeps-it-cool-with-ice.html
    
Entenda o que é DevSecOps

     http://www.devsecops.org/blog/2015/2/15/what-is-devsecops    
    
## 3. ROTEIRO DE ESTUDOS

Essa sugestão de estudos é baseada no CAMS do Willis e Edwards.

### 3.1  Cultura

- Estude a metodologia ágil de desenvovlimento
- Estude metodologias ágeis em geral
- Estude Scrum
- Estude Kanban
- Estude Lean
- Entenda o que é uma cultura de colaboração e feedback.

Faça um exercicio e tente enxergar os principais problemas culturais do 
seu time/organização e encontre uma forma corrigir tais problemas adaptando esses métodos.

### 3.2 Automação

- Estude **Virtualização**
- Estude **Cloud computing**
- Estude Mark Burgess e **Gerência de configurações**
- Estude **Infraestrutura as Code**
- Conheça pelo menos duas ferramentas de Gerência de Configuração (CFEngine e Puppet)
- Conheça pelo menos duas ferramentas de Orquestração (Fabric e Ansible)
- Entenda o que é VCS (Version Control System)
- Estude **GIT** pra valer
- Estude workflows de desenvolvimento usando GIT (olhe os projetos opensource)
- Estude Continuous Integration
- Estude Continuous Delivery
- Estude Continuous Deployment
- Estude TDD/BDD/ATDD
- Estude Load Testing
- Estude Stress Testing
- Estude Security Testing
- Estude Containers e Docker
- Estude Microservicos
- Estude ferramentas para fazer provisionamento automatizado
- Estude ferramentas de autoserviço
- Estude sistemas Unix e Linux
- Estude networking
- Estude e domine pelo menos 2 linguagens interpretadas (Ruby e Python)
- Conheça pelo menos 1 stack de cada linguagem
- Estude a linguagem shell

Labs sugeridos

- Tente subir uma stack full de automação (ex. puppet)
  - Tente automatizar confs simples do OS (ex. centos)
  - Tente automatizar a instalacao e configuracao de um APP (ex. apache)
- Tente provisionar VMs ou Containers de forma automatizada via autoserviço 
- Tente construir um pipeline de entrega
  - Indo desde o commit, passando por build, testes até o deploy em vm e container 

### 3.3 Avaliação/Métricas

- Entenda o que são métricas e pq são importantes
- Estude real-time-metrics
- Estude ferramentas APM
- Conheca bem pelo menos duas ferramentas de monitoramento (Nagios/Zabbix)
- Estude algum stack para tratamento de logs (ELK)
- Estude algum stack para coleta e armazenamento métricas (Collectd/Statsd/Graphite/Graphana)

Labs sugeridos

- Tente subir uma stack full de gestao de logs (ex. ELK)
  - Coloque diversos APPs para jogar dados lá
- Tente subir um stack full de gestao de dados
  - Gere dados, colete, armazene e visualize com essa stack (ex. Graphite/Graphana)

### 3.4 Compartilhamento

- Entenda a cultura de dividir responsabilidades
- Entenda a cultura Blameless
- Aprenda a compartilhar ferramentas entre todos os times
- Aprenda a compartilhar código entre todos os times
- Aprenda a compartilhar informações e dados
- Procure sempre melhorar o processo de comunicação 
- Entenda como funcionam times de produtos
- Ofereça feedback constante entre os times
- Volte pra cultura  e reinicie o ciclo até aqui
- Entenda e pratique Dojos
- Entenda os Hackerspaces
- Entenda os Hackatons

----

Cada eixo desse do roteiro de estudos tem cursos disponíveis no mercado, documentação farta online, artigos variados na rede.

Evite procurar formação DevOps, o ideal é que crie o seu roteiro de estudos.

Se encontrar formação DevOps por ai, tome muito cuidado para não ser uma empresa inventando algo só para faturar em cima da Buzzword.

Como voce viu DevOps é algo amplo, dificil de colocar em um curso de 24 ou 40 horas.

Agora é começar os estudos, divirta-se!

Adaptado By Guto Carvalho